
$(document).ready(function() {
	
	$(".datepicker").datepicker({
		autoclose: true,
		todayHighlight: true
	}).datepicker('update', new Date());
	
	var $searchbar  = $('#searchbar');
	
	$('#searchtoggl').on('click', function(e){
		e.preventDefault();
		if(!$searchbar.is(":visible")) {
			$('#searchtoggl span').addClass('active');
		} else {
			$('#searchtoggl span').removeClass('active');
		}
		
		$searchbar.slideToggle(300, function(){
			// callback after search bar animation
		});
	});
	
	$('#searchform').submit(function(e){
		e.preventDefault(); // stop form submission
	});
	
	function setHeight (item, width, height) {
		var itemWidth = item.width();
		item.css({
			height: (itemWidth / width) * height
		});
	}
	setHeight($('#myCarousel .img-wrapper'), 760, 362);
	setHeight($('.news-list .img-wrapper'), 19, 8);
	setHeight($('#myCarousel1 .img-wrapper'), 41, 20);
	setHeight($('.lightgallery a'), 1, 1);
	setHeight($('.tenders-list li .img-wrapper'), 17, 8);
	
	var elems = $('.gallery-img-wrapper .img-responsive');
	elems.each(function(){
		var elem = $(this);
		var width = elem.width();
		var height = elem.height();
		if(width < height){
			elem.addClass('element_vertical');
		}else{
			elem.addClass('element_horizont');
		}
	})
	
	
});




