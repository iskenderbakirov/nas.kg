/**
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

// DO NOT EDIT THIS GENERATED OUTPUT DIRECTLY!
// This file should be overwritten as part of your build process.
// If you need to extend the behavior of the generated service worker, the best approach is to write
// additional code and include it using the importScripts option:
//   https://github.com/GoogleChrome/sw-precache#importscripts-arraystring
//
// Alternatively, it's possible to make changes to the underlying template file and then use that as the
// new base for generating output, via the templateFilePath option:
//   https://github.com/GoogleChrome/sw-precache#templatefilepath-string
//
// If you go that route, make sure that whenever you update your sw-precache dependency, you reconcile any
// changes made to this original template file with your modified copy.

// This generated service worker JavaScript will precache your site's resources.
// The code needs to be saved in a .js file at the top-level of your site, and registered
// from your pages in order to be used. See
// https://github.com/googlechrome/sw-precache/blob/master/demo/app/js/service-worker-registration.js
// for an example of how you can register this script and handle various service worker events.

/* eslint-env worker, serviceworker */
/* eslint-disable indent, no-unused-vars, no-multiple-empty-lines, max-nested-callbacks, space-before-function-paren, quotes, comma-spacing */
'use strict';

var precacheConfig = [["/_content.html","d41d8cd98f00b204e9800998ecf8427e"],["/bank.html","a50350d6db25fca42bf61700e9354b16"],["/css/bootstrap.min.css","e672c9a1cdd414aa32ad9ade6ddc8415"],["/css/style.min.css","8463266a974e09f9b7f4c4682e45b009"],["/fonts/glyphicons-halflings-regular.eot","f4769f9bdb7466be65088239c12046d1"],["/fonts/glyphicons-halflings-regular.svg","89889688147bd7575d6327160d64e760"],["/fonts/glyphicons-halflings-regular.ttf","e18bbf611f2a2e43afc071aa2f4e1512"],["/fonts/glyphicons-halflings-regular.woff","fa2772327f55d8198301fdb8bcfc8158"],["/fonts/glyphicons-halflings-regular.woff2","448c34a56d699c29117adc64c43affeb"],["/fonts/lg.eot","ecff11700aad0000cf3503f537d1df17"],["/fonts/lg.svg","98d62b1e5f5b556facf319b19c6c7cba"],["/fonts/lg.ttf","4fe6f9caff8b287170d51d3d71d5e5c6"],["/fonts/lg.woff","5fd4c338c1a1b1eeeb2c7b0a0967773d"],["/forum.html","8069ec74e7866c1ddae14d218c419555"],["/img/favicons/android-icon-144x144.png","49d9a0fb8227483c0dfe09d912f10bba"],["/img/favicons/android-icon-192x192.png","d3e4973db7b7913b93887d4df20ae069"],["/img/favicons/android-icon-36x36.png","68cf3f39912e558aa0f107747d8ef8d6"],["/img/favicons/android-icon-48x48.png","60ea79160abe3f79e3f8bf94ca798eb1"],["/img/favicons/android-icon-72x72.png","2f18a7e561c2ef2d7349e936ad87c199"],["/img/favicons/android-icon-96x96.png","08ab812ff28fde0c8cf94e2fefe5f57d"],["/img/favicons/apple-icon-114x114.png","7e9471db0b374916a8f295262ed606b9"],["/img/favicons/apple-icon-120x120.png","f4a5f60987cee4363aad22c169f0c655"],["/img/favicons/apple-icon-144x144.png","49d9a0fb8227483c0dfe09d912f10bba"],["/img/favicons/apple-icon-152x152.png","d0f761fd4fb7956876381d93873add5b"],["/img/favicons/apple-icon-180x180.png","224633c799c36bf4fa5fc7aa5cfa262b"],["/img/favicons/apple-icon-57x57.png","43e88cc02b5cee04fc4cd8fe674d1eb3"],["/img/favicons/apple-icon-60x60.png","4a59129f365e278325f7cf1edc21dfc8"],["/img/favicons/apple-icon-72x72.png","2f18a7e561c2ef2d7349e936ad87c199"],["/img/favicons/apple-icon-76x76.png","98a28b698c375be8865936c0312abac5"],["/img/favicons/apple-icon-precomposed.png","294d655a643b658c2d63dd37c4864108"],["/img/favicons/apple-icon.png","294d655a643b658c2d63dd37c4864108"],["/img/favicons/browserconfig.xml","653d077300a12f09a69caeea7a8947f8"],["/img/favicons/favicon-16x16.png","d252585654587941bfefb55e7e618c69"],["/img/favicons/favicon-32x32.png","e561e2e155c0b2f7f82b34b562fd4189"],["/img/favicons/favicon-96x96.png","08ab812ff28fde0c8cf94e2fefe5f57d"],["/img/favicons/favicon.ico","b5cf92b6338fb58eca2c8aeabc86c1e0"],["/img/favicons/manifest.json","b58fcfa7628c9205cb11a1b2c3e8f99a"],["/img/favicons/ms-icon-144x144.png","49d9a0fb8227483c0dfe09d912f10bba"],["/img/favicons/ms-icon-150x150.png","53638d47ed377a3ed02ee39e8c88f1e8"],["/img/favicons/ms-icon-310x310.png","81638b757e1a4bb5e115bd6087a30d6f"],["/img/favicons/ms-icon-70x70.png","52fe4b68003783d2052d0884c6396cda"],["/img/gallery/1-1600.jpg","22cb5b23d69bb1adbcd50efa4e3c6b40"],["/img/gallery/1-375.jpg","c42a1ede0a57736f2b56d1c1b02f20d4"],["/img/gallery/1-480.jpg","13f0bac97e8858217b107f5d49da336a"],["/img/gallery/1.jpg","4f2dcbf1a8e8ada5904e634873a2ea5c"],["/img/gallery/13-1600.jpg","e2b13b9806b8c2813502caa502deb397"],["/img/gallery/13-375.jpg","09c13d3131050e3f21e4a94a05d0ee4d"],["/img/gallery/13-480.jpg","54571bc02663ec1b1be5fbe25a0554ba"],["/img/gallery/13.jpg","d7c880ef14801eb070f6b19d320461b8"],["/img/gallery/2-1600.jpg","cd8acbfbf54d352c94fb3f55511c4bc6"],["/img/gallery/2-375.jpg","3554f79657c0fae37e928ce3c596bf8d"],["/img/gallery/2-480.jpg","3298636106d895b9a5c1c4fedd935169"],["/img/gallery/2.jpg","8a4108b7dd47f92f6aabfe2fd46ce3f6"],["/img/gallery/4-1600.jpg","9d6ab0567f3496f925077ce97500b5cd"],["/img/gallery/4-375.jpg","35ae2b7c51c6de72ee597d3b61a60df5"],["/img/gallery/4-480.jpg","97379a9ba5141892ba841fc658850ce5"],["/img/gallery/4.jpg","649f84ca6cea5a0a5d57ad8684aff210"],["/img/gallery/thumb-1.jpg","5b5a83892d84e790e7be764307a4a73c"],["/img/gallery/thumb-13.jpg","c4ca0f2dd8becd2b768cfb195c9bcf39"],["/img/gallery/thumb-2.jpg","d3f64cc714459377e908ac13d8d0208a"],["/img/gallery/thumb-4.jpg","7a3101f2eb96a3223cf11f05b3141c6c"],["/img/icons/fb.svg","f686ded752c61c286e9f9aec7050f1a5"],["/img/icons/lang_en.svg","5e3b6519e6e8f7eea3d8e7b84d4bf167"],["/img/icons/lang_kg.svg","3f14456f3232cfc9e03c9878d77e2598"],["/img/icons/lang_ru.svg","1b5d7f1921ba1e044789f5437a59b952"],["/img/icons/user.svg","9a97df82344b670f1d76efd6ba57402c"],["/img/jpg/bplan2-clothes.zip","9d47758cf913ce1fbf4a09645faf3450"],["/img/jpg/news-item.jpg","2862167074c94a9b172ed4bba9b4f65c"],["/img/jpg/photo-gallery.jpg","3613571d2d67d29bf7ee9a566004ff86"],["/img/jpg/photo-gallery1.jpg","f5792064b8e7f1e76dfd4922caa6e34c"],["/img/jpg/photo-gallery2.jpg","de8178c4b326bb60c9bff7c813a2b817"],["/img/jpg/press-item.jpg","d98deeb5cdd7456a04260c4e8b6852d9"],["/img/jpg/tender.jpg","91e5dbc2b51f57f2d31b650c75376a2e"],["/img/jpg/Б®І≠•б ѓЂ†≠ ђ†£†І®≠ Ѓ§•¶§л.pdf","26dcab053da7990d598f3d9174e14642"],["/img/loading.gif","bbdac9cda255c54bfd809110aff87898"],["/img/logo.svg","33d3863bee89fd7d7374acbfcf1095d6"],["/img/mitapp.svg","529787e76ae35ead637a338b99ba011a"],["/index.html","04f14a629f2e05dd84a1230ef5eef3da"],["/js/bootstrap.min.js","f78e57eedc67e2e1ae34815833e7eec3"],["/js/common.min.js","70c265e4fc7d668eba7a7cbdd4217ebf"],["/js/jquery-ui-1.9.2.min.js","7368211102cd69dfb5930379c7948a0e"],["/js/jquery.mousewheel.min.js","639d1c35a685d111aa4a509a2dbf660c"],["/js/libs.min.js","f70dd4394e45837cbae608cc6a3cacb7"],["/js/mixitup.min.js","20c383399604b2b09cfc9c434f2465af"],["/lightGallery/demo/index.html","fb946ea56d2f25644a204450e19ccd22"],["/lightGallery/test/lightgallery.html","f94a02e19c5638ca30c09dbbb2214f01"],["/manifest.json","868a2a1a77f23a12c3b779bb26834da2"],["/news-single.html","e89da305715bd379525ecd8d4113b545"],["/news.html","27b9d24d0f40b2c76433c5f5ee70d663"],["/tenders-single.html","faa6bbb2b7d995e5b5eaabba90e9d1e2"],["/tenders.html","cc4b7a34a93153525cc2c9ad9663d55b"],["/text.html","feb899e77cdccf02754072f53f6e3b6d"]];
var cacheName = 'sw-precache-v3--' + (self.registration ? self.registration.scope : '');


var ignoreUrlParametersMatching = [/^utm_/];



var addDirectoryIndex = function(originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
      url.pathname += index;
    }
    return url.toString();
  };

var cleanResponse = function(originalResponse) {
    // If this is not a redirected response, then we don't have to do anything.
    if (!originalResponse.redirected) {
      return Promise.resolve(originalResponse);
    }

    // Firefox 50 and below doesn't support the Response.body stream, so we may
    // need to read the entire body to memory as a Blob.
    var bodyPromise = 'body' in originalResponse ?
      Promise.resolve(originalResponse.body) :
      originalResponse.blob();

    return bodyPromise.then(function(body) {
      // new Response() is happy when passed either a stream or a Blob.
      return new Response(body, {
        headers: originalResponse.headers,
        status: originalResponse.status,
        statusText: originalResponse.statusText
      });
    });
  };

var createCacheKey = function(originalUrl, paramName, paramValue,
                           dontCacheBustUrlsMatching) {
    // Create a new URL object to avoid modifying originalUrl.
    var url = new URL(originalUrl);

    // If dontCacheBustUrlsMatching is not set, or if we don't have a match,
    // then add in the extra cache-busting URL parameter.
    if (!dontCacheBustUrlsMatching ||
        !(url.pathname.match(dontCacheBustUrlsMatching))) {
      url.search += (url.search ? '&' : '') +
        encodeURIComponent(paramName) + '=' + encodeURIComponent(paramValue);
    }

    return url.toString();
  };

var isPathWhitelisted = function(whitelist, absoluteUrlString) {
    // If the whitelist is empty, then consider all URLs to be whitelisted.
    if (whitelist.length === 0) {
      return true;
    }

    // Otherwise compare each path regex to the path of the URL passed in.
    var path = (new URL(absoluteUrlString)).pathname;
    return whitelist.some(function(whitelistedPathRegex) {
      return path.match(whitelistedPathRegex);
    });
  };

var stripIgnoredUrlParameters = function(originalUrl,
    ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);
    // Remove the hash; see https://github.com/GoogleChrome/sw-precache/issues/290
    url.hash = '';

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function(kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function(kv) {
        return ignoreUrlParametersMatching.every(function(ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function(kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
  };


var hashParamName = '_sw-precache';
var urlsToCacheKeys = new Map(
  precacheConfig.map(function(item) {
    var relativeUrl = item[0];
    var hash = item[1];
    var absoluteUrl = new URL(relativeUrl, self.location);
    var cacheKey = createCacheKey(absoluteUrl, hashParamName, hash, false);
    return [absoluteUrl.toString(), cacheKey];
  })
);

function setOfCachedUrls(cache) {
  return cache.keys().then(function(requests) {
    return requests.map(function(request) {
      return request.url;
    });
  }).then(function(urls) {
    return new Set(urls);
  });
}

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return setOfCachedUrls(cache).then(function(cachedUrls) {
        return Promise.all(
          Array.from(urlsToCacheKeys.values()).map(function(cacheKey) {
            // If we don't have a key matching url in the cache already, add it.
            if (!cachedUrls.has(cacheKey)) {
              var request = new Request(cacheKey, {credentials: 'same-origin'});
              return fetch(request).then(function(response) {
                // Bail out of installation unless we get back a 200 OK for
                // every request.
                if (!response.ok) {
                  throw new Error('Request for ' + cacheKey + ' returned a ' +
                    'response with status ' + response.status);
                }

                return cleanResponse(response).then(function(responseToCache) {
                  return cache.put(cacheKey, responseToCache);
                });
              });
            }
          })
        );
      });
    }).then(function() {
      
      // Force the SW to transition from installing -> active state
      return self.skipWaiting();
      
    })
  );
});

self.addEventListener('activate', function(event) {
  var setOfExpectedUrls = new Set(urlsToCacheKeys.values());

  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.keys().then(function(existingRequests) {
        return Promise.all(
          existingRequests.map(function(existingRequest) {
            if (!setOfExpectedUrls.has(existingRequest.url)) {
              return cache.delete(existingRequest);
            }
          })
        );
      });
    }).then(function() {
      
      return self.clients.claim();
      
    })
  );
});


self.addEventListener('fetch', function(event) {
  if (event.request.method === 'GET') {
    // Should we call event.respondWith() inside this fetch event handler?
    // This needs to be determined synchronously, which will give other fetch
    // handlers a chance to handle the request if need be.
    var shouldRespond;

    // First, remove all the ignored parameters and hash fragment, and see if we
    // have that URL in our cache. If so, great! shouldRespond will be true.
    var url = stripIgnoredUrlParameters(event.request.url, ignoreUrlParametersMatching);
    shouldRespond = urlsToCacheKeys.has(url);

    // If shouldRespond is false, check again, this time with 'index.html'
    // (or whatever the directoryIndex option is set to) at the end.
    var directoryIndex = 'index.html';
    if (!shouldRespond && directoryIndex) {
      url = addDirectoryIndex(url, directoryIndex);
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond is still false, check to see if this is a navigation
    // request, and if so, whether the URL matches navigateFallbackWhitelist.
    var navigateFallback = '';
    if (!shouldRespond &&
        navigateFallback &&
        (event.request.mode === 'navigate') &&
        isPathWhitelisted([], event.request.url)) {
      url = new URL(navigateFallback, self.location).toString();
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond was set to true at any point, then call
    // event.respondWith(), using the appropriate cache key.
    if (shouldRespond) {
      event.respondWith(
        caches.open(cacheName).then(function(cache) {
          return cache.match(urlsToCacheKeys.get(url)).then(function(response) {
            if (response) {
              return response;
            }
            throw Error('The cached response that was expected is missing.');
          });
        }).catch(function(e) {
          // Fall back to just fetch()ing the request if some unexpected error
          // prevented the cached response from being valid.
          console.warn('Couldn\'t serve response for "%s" from cache: %O', event.request.url, e);
          return fetch(event.request);
        })
      );
    }
  }
});







